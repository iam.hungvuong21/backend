const productsModels = require("../model/products");
const productsController = {};

productsController.DeleteProductsById = async (req, res) => {
  try {
    const idPrams = req.params.id;
    await productsModels.deleteOne({ _id: idPrams });

    return res.status(200).json({
      status: 200,
      message: "Delete Products Success",
    });
  } catch (error) {
    return res.status(500).json({
      status: 500,
      message: "Error Server",
    });
  }
};

productsController.CreateProducts = async (req, res) => {
  try {
    let { title, description, price, imageUrl } = req.body;

    let data = await productsModels.create({
      title: title,
      description: description,
      price: price,
      imageUrl: imageUrl,
    });

    if (data) {
      return res.status(200).json({
        status: 200,
        message: "Create Success Products",
        data: data,
      });
    }

    return res.status(400).json({
      status: 400,
      message: "Create Failed Products ",
    });
  } catch (error) {
    return res.status(500).json({
      status: 500,
      message: "Error Server",
    });
  }
};

productsController.getAllProducts = async (req, res) => {
  try {
    const products = await productsModels.find({});

    if (products) {
      return res.status(200).json({
        status: 200,
        message: "Get All Success",
        data: products,
      });
    }

    return res.status(400).json({
      status: 400,
      message: "Get All Failed",
    });
  } catch (error) {
    return res.status(500).json({
      status: 500,
      message: "Error Server",
    });
  }
};

productsController.GetProductsById = async (req, res) => {
  try {
    let id = req.params.id;
    await productsModels.findOne({ _id: id });

      return res.status(200).json({
        status: 200,
        message: "Get Products By ID Success",
        data: data,
      });

  } catch (error) {
    return res.status(500).json({
      status: 500,
      message: "Error Server",
    });
  }
};

productsController.UpdateProductsById = async (req, res) => {
  try {
    let id = req.params.id;
    let { title, price, description, imageUrl, amount } = req.body;

    const data = await productsModels.updateOne(
      {
        _id: id,
      },
      {
        title,
        description,
        price,
        imageUrl,
        amount,
      }
    );

    if (data) {
      return res.status(200).json({
        status: 200,
        message: "Update Products Success",
        data: data,
      });
    }

    return res.status(400).json({
      status: 400,
      message: "Update Products ",
    });
  } catch (error) {
    return res.status(500).json({
      status: 500,
      message: "Error Server",
    });
  }
};

module.exports = productsController;
