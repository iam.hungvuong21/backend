const mongoose = require("../config/dbConnect")
const AccountsSchema = new mongoose.Schema({
    username: String,
    email: String,
    password: String
},{
    collection:"accounts"
})

const Accounts = mongoose.model("accounts", AccountsSchema)
module.exports = Accounts