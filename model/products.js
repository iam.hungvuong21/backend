const mongoose = require("../config/dbConnect")
const productsSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true,
    },
    price: {
        type: Number,
        required: true
    },
    imageUrl:{
        type: String,
        required: true
    },
    amount: {
        type: String,
        default: 0
    }
},{
    collection:"products"
})

const Products = mongoose.model("products", productsSchema)
module.exports = Products