const express = require("express")
const router = express.Router()
const productsController = require("../controller/products")

router.post("/", productsController.CreateProducts)
router.get("/get", productsController.getAllProducts)
router.get("/get", productsController.GetProductsById)
router.put("/update/:id", productsController.UpdateProductsById)
router.delete("/get/:id", productsController.DeleteProductsById)


module.exports = router



